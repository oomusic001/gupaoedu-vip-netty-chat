package com.gupao.vip.netty.chat.protocol;

/**
 * 聊天协议
 *
 */
public enum IMP {
    /*系统消息*/
    SYSTEM("SYSTEM"),
    /*登出指令*/
    LOGIN("LOGIN"),
    LOGOUT("LOGOUT"),
    /*聊天消息*/
    CHAT("CHAT"),
    /*送鲜花*/
    FLOWER("FLOWER");
    private  String name;
    IMP(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public static boolean isIMP(String content){
        return  content.matches("^\\[(|SYSTEM|LOGIN|CHAT|FLOWER)\\]");
    }
    public IMP setName(String name) {
        this.name = name;
        return this;
    }
}

