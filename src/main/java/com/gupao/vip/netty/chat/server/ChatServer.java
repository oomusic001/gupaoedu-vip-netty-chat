package com.gupao.vip.netty.chat.server;

import com.gupao.vip.netty.chat.protocol.IMDecoder;
import com.gupao.vip.netty.chat.protocol.IMEncoder;
import com.gupao.vip.netty.chat.server.handler.HttpServerHandler;
import com.gupao.vip.netty.chat.server.handler.TerminalServerHandler;
import com.gupao.vip.netty.chat.server.handler.WebSocketServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

import java.io.IOException;

public class ChatServer {

    private  int port = 9999;
    public void start(int port){
        //主从线程模型的线程池
        EventLoopGroup bossGroup = new NioEventLoopGroup(); //调用父类的构造方法进行初始化。
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            //相当于ServerSocket监听
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            //第三个参数指定ServerSocketChannel关心的事件类型为SelectionKey.OP_ACCEPT  todo

            //bossGroup对应AbstractBootstrap  负责轮询？
            //workerGroup对应ServerBootstrap  负责请求事件的处理。
            serverBootstrap.group(bossGroup,workerGroup)
                    //在进行服务端开发时，必须通过ServerBootstrap引导类的channel方法来指定channel类型，
                    // channel方法的调用其实就是实例化了一个用于生成此channel类型对象的工厂对象。
                    // 并且在bind调用后，会调用此工厂对象来生成一个新channel
                    .channel(NioServerSocketChannel.class) //指定channel工厂生成的channel类型：nioServerSocketChannel
                    //进行http的配置。socket的标准参数。BACKLOG用于构造服务端套接字ServerSocket对象，标识当服务器请求处理线程全满时，
                    // 用于临时存放已完成三次握手的请求的队列的最大长度。如果未设置或所设置的值小于1，Java将使用默认值50。
                    .option(ChannelOption.SO_BACKLOG,1024)
                    //分配一个子线程来执行应用逻辑，包括执行的顺序，执行的handler(自定义)
                    //需要把执行的逻辑进行封装，封装为一个pipeline对象。
                    //下面是加入各种各样的编解码器和协议的逻辑处理
                    .childHandler(new ChannelInitializer<SocketChannel>() { //ChannelHandler  即channel处理器
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            //这里不知道客户端传来的是什么，所以需要进行编码(用自定义的编码器)
                            //解析自定义协议

                            /**
                             * 当服务端收到消息后，并不会知道发送过来的是是什么消息，不知道是需要编码还是需要解码。
                             * 所以需要假如多种协议，来尝试进行编码或者解码，当第一个编解码器不认识这个消息，则传给下一个编解码器。
                             * 直到匹配上之后，才不会进行传递，而是通过自己默认的协议处理器，或自定义的协议处理器(编码解码器)进行编解码。
                             *
                             */

                            pipeline.addLast(new IMDecoder());
                            pipeline.addLast(new IMEncoder());
                            //专门用来处理直接发送IMMessage对的idea控制台。通过编码解码器识别是是什么协议。
                            pipeline.addLast(new TerminalServerHandler()); //协议的逻辑处理。


                            //解析http请求
                            pipeline.addLast(new HttpServerCodec());
                            //将一个http请求或响应的多个消息对象变成一个fullHttpRequest完整的消息对象。
                            pipeline.addLast(new HttpObjectAggregator(64 * 1024));
                            //主要用于处理大数据流，比如一个1g的大小的文件如果直接传输肯定会撑爆jvm内存，加上handler可以解决
                            pipeline.addLast(new ChunkedWriteHandler()); //Inbound , Outbound
                            //直接用来处理web页面的handler。解析http协议的。通过编码解码器识别是是什么协议。
                            pipeline.addLast(new HttpServerHandler()); //Inbound 协议的逻辑处理。


                            //解析webSocket请求
                            pipeline.addLast(new WebSocketServerProtocolHandler("/im"));
                            //用来处理websocket通信协议。通过编码解码器识别是是什么协议。
                            pipeline.addLast(new WebSocketServerHandler()); //Inbound  协议的逻辑处理。

                        }
                    });//分配执行逻辑结束
                ChannelFuture future =serverBootstrap.bind(this.port).sync(); //启动时会检查bosGroup是否为空
                System.out.println("服务已经启动，监听端口:"+port);
                future.channel().closeFuture().sync();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }

    }

    public void start(){
        start(this.port);
    }
    public static void main(String[] args) throws IOException {
        if(args.length > 0) {
            new ChatServer().start(Integer.valueOf(args[0]));
        }else{
            new ChatServer().start();
        }
    }
}
