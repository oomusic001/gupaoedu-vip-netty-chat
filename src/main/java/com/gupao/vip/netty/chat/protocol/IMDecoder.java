package com.gupao.vip.netty.chat.protocol;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.apache.commons.lang.StringUtils;
import org.msgpack.MessagePack;
import org.msgpack.MessageTypeException;

import  java.lang.*;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 解码器：对IMMessage进行解码，解码为自己能够认识的对象
 * inbound
 */
public class IMDecoder extends ByteToMessageDecoder{//不用泛型，write出去的，直接new对象。
    //消息头 -
    private Pattern pattern = Pattern.compile("^\\[(.*)\\](\\s\\-\\s(.*))?");

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        try{
            //从缓存中拿到信息
            final int length = in.readableBytes();
            final byte[] array = new byte[length];
            String content = new String(array, in.readerIndex(),length);

            //空消息不解析
            if ((null == content || "".equals(content.trim()))){
                if (!IMP.isIMP(content)){
                    ctx.channel().pipeline().remove(this);
                    return;
                }
            }
            //把字符串变成一个我们能够是被的IMMessage对象
            in.getBytes(in.readerIndex(), array, 0, length);
            //利用序列化框架，将网络流信息直接转化为IMMessage对象。
            out.add(new MessagePack().read(array,IMMessage.class));
            in.clear();
        }catch(MessageTypeException e){
            ctx.channel().pipeline().remove(this);
        }
    }
    public IMMessage decode(String msg){
        if (null == msg ||"".equals(msg.trim())) return  null;
        //解析字符串最好的办法是正则 把字符串变成我们想要的信息。
        final Matcher m = pattern.matcher(msg);
        //消息头
        String header = "";
        //消息体
        String content = "";
        if (m.matches()){
            header = m.group(1);
            content = m.group(2);
        }
        String [] headers = header.split("\\]\\[");

        //时间
        long time = Long.parseLong(headers[1]);
        //昵称
        String nickName  = headers[2];
        //昵称最大十个字
        nickName = nickName.length()<10?nickName:nickName.substring(0,9);
        //登录没有 content
        if(msg.startsWith("[" + IMP.LOGIN.getName() + "]")){
            return new IMMessage(headers[0],nickName,time,headers[3]);
            //聊天没有终端
        }else if(msg.startsWith("[" + IMP.CHAT.getName() + "]")){
            return new IMMessage(headers[0],time,nickName,content);
            //送花没有content
        }else if(msg.startsWith("[" + IMP.FLOWER.getName() + "]")){
            return new IMMessage(headers[0],nickName,time,headers[3]);
        }else{
            return null;
        }
    }




























































}
