package com.gupao.vip.netty.chat.processor;

import com.alibaba.fastjson.JSONObject;
import com.gupao.vip.netty.chat.protocol.IMDecoder;
import com.gupao.vip.netty.chat.protocol.IMEncoder;
import com.gupao.vip.netty.chat.protocol.IMMessage;
import com.gupao.vip.netty.chat.protocol.IMP;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import io.netty.util.concurrent.ImmediateEventExecutor;


public class MsgProcessor {
    //定义一些扩展属性
    public static final AttributeKey<String> NICK_NAME = AttributeKey.valueOf("nickName");
    public static final AttributeKey<String> IP_ADDR = AttributeKey.valueOf("ipAddr");
    public static final AttributeKey<JSONObject> ATTRS = AttributeKey.valueOf("attrs");
    public static final AttributeKey<String> FROM = AttributeKey.valueOf("from");
    //保存在线用户的容器。线程安全
    private static ChannelGroup onlineUsers = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private IMDecoder decoder = new IMDecoder();
    private IMEncoder encoder = new IMEncoder();
    /**
     * 消息处理
     */
    public void process(Channel client, IMMessage msg){ //表示控制台拿到的
        process(client,encoder.encode(msg));
    }

    public void process(Channel client, String msg){ //表示socket拿到的消息
        IMMessage request = decoder.decode(msg);
        String addr = getAddress(client);
        if (request == null) return;
        //登录操作
        if (request.getCmd().equals(IMP.LOGIN.getName())){
            //保存ip,昵称,终端类型
            client.attr(NICK_NAME).getAndSet(request.getSender());
            client.attr(IP_ADDR).getAndSet(addr);
            client.attr(FROM).getAndSet(request.getTerminal());
            //把这个用户保存到统一的容器中，要给所有在线的推送消息。
            onlineUsers.add(client);//保存在线用户

            //通知所有的用户，谁谁上线了
            for (Channel channel : onlineUsers) {
                boolean isSelf = (channel == client);
                //如果是自己，告诉自己已经上线
                if(!isSelf){
                    request = new IMMessage(IMP.SYSTEM.getName(), sysTime(),
                            onlineUsers.size(), getNickName(client) + "加入");
                }else{
                    request = new IMMessage(IMP.SYSTEM.getName(), sysTime(),
                            onlineUsers.size(), "已与服务器建立连接！");
                }
                //消息准备好了，要开始推送消息了。
                //如果终端是控制台，推IMMessage。
                if("Console".equals(channel.attr(FROM).get())){
                    channel.writeAndFlush(request);
                    continue;
                }
                //如果终端是webSocket，推支持webSocket的自定义协议字符串
                String content = encoder.encode(request);
                channel.writeAndFlush(new TextWebSocketFrame(content));
            }

        }else if (request.getCmd().equals(IMP.CHAT.getName())){
            for (Channel channel : onlineUsers) {
                boolean isself = (channel == client);
                if (isself) {
                    request.setSender("you");
                }else{
                    request.setSender(getNickName(client));
                }
                request.setTime(sysTime());

                if("Console".equals(channel.attr(FROM).get()) & !isself){
                    channel.writeAndFlush(request);
                    continue;
                }
                String content = encoder.encode(request);
                channel.writeAndFlush(new TextWebSocketFrame(content));
            }
        }else if (request.getCmd().equals(IMP.FLOWER.getName())){
            JSONObject attrs = getAttrs(client);
            long currTime = sysTime();
            //属于权限，可以自己加一个handler进行控制。
            if(null != attrs){
                long lastTime = attrs.getLongValue("lastFlowerTime");
                //60秒之内不允许重复刷鲜花
                int secends = 10;
                long sub = currTime - lastTime;
                if(sub < 1000 * secends){
                    request.setSender("you");
                    request.setCmd(IMP.SYSTEM.getName());
                    request.setContent("您送鲜花太频繁," + (secends - Math.round(sub / 1000)) + "秒后再试");
                    //只有web页面支持刷花
                    String content = encoder.encode(request);
                    client.writeAndFlush(new TextWebSocketFrame(content));
                    return;
                }
            }

            //正常送花
            for (Channel channel : onlineUsers) {
                if (channel == client) {
                    request.setSender("you");
                    request.setContent("你给大家送了一波鲜花雨");
                    setAttrs(client, "lastFlowerTime", currTime);
                }else{
                    request.setSender(getNickName(client));
                    request.setContent(getNickName(client) + "送来一波鲜花雨");
                }
                request.setTime(sysTime());

                String content = encoder.encode(request);
                channel.writeAndFlush(new TextWebSocketFrame(content));
            }
        }
    }
    /**
     * 获取用户昵称
     * @param client
     * @return
     */
    public String getNickName(Channel client){
        return client.attr(NICK_NAME).get();
    }
    /**
     * 获取用户远程IP地址
     * @param client
     * @return
     */
    public String getAddress(Channel client){
        return client.remoteAddress().toString().replaceFirst("/","");
    }

    /**
     * 获取扩展属性
     * @param client
     * @return
     */
    public JSONObject getAttrs(Channel client){
        try{
            return client.attr(ATTRS).get();
        }catch(Exception e){
            return null;
        }
    }
    /**
     * 获取系统时间
     * @return
     */
    private Long sysTime(){
        return System.currentTimeMillis();
    }
    /**
     * 获取扩展属性
     * @param client
     * @return
     */
    private void setAttrs(Channel client,String key,Object value){
        try{
            JSONObject json = client.attr(ATTRS).get();
            json.put(key, value);
            client.attr(ATTRS).set(json);
        }catch(Exception e){
            JSONObject json = new JSONObject();
            json.put(key, value);
            client.attr(ATTRS).set(json);
        }
    }

    /**
     * 登出通知
     * @param client
     */
    public void logout(Channel client){
        //如果nickName为null，没有遵从聊天协议的连接，表示未非法登录
        if(getNickName(client) == null){ return; }
        for (Channel channel : onlineUsers) {
            IMMessage request = new IMMessage(IMP.SYSTEM.getName(), sysTime(), onlineUsers.size(), getNickName(client) + "离开");
            String content = encoder.encode(request);
            channel.writeAndFlush(new TextWebSocketFrame(content));
        }
        onlineUsers.remove(client);
    }
}
