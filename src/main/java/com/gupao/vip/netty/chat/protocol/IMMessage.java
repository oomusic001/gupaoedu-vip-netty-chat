package com.gupao.vip.netty.chat.protocol;

import lombok.Data;
import org.msgpack.annotation.Message;

/**
 * 消息对象。 需要编码、解码器，对自己发的信息进行编码为对方能认识的对象。
 *          把别人发来的信息解码为自己能认识的对象。
 */
@Message
@Data
public class IMMessage {
    private String addr; //IP地址及端口
    private String cmd;//命令类型：LOGIN SYSTEM LOGOUT
    private long time;//命令发送时间
    private int online;//当前在线人数
    private String sender;//发送人
    private String receiver;//接收人
    private String content;//消息内容
    private String terminal;//终端  : 手机，控制台，网页，电脑

    public IMMessage() {
    }



    //LOGIN
    public IMMessage(String cmd,String sender,long time,String terminal){
        this.cmd = cmd;
        this.content = content;
        this.time = time;
        this.sender = sender;
        this.terminal = terminal;
    }



    public IMMessage(String cmd, long time, String sender, String content) {
        this.cmd = cmd;
        this.time = time;
        this.sender = sender;
        this.content = content;
    }

    public IMMessage(String cmd, long time, int online, String content) {
        this.cmd = cmd;
        this.time = time;
        this.online = online;
        this.content = content;
    }

    public String getAddr() {
        return addr;
    }

    public IMMessage setAddr(String addr) {
        this.addr = addr;
        return this;
    }

    public String getCmd() {
        return cmd;
    }

    public IMMessage setCmd(String cmd) {
        this.cmd = cmd;
        return this;
    }

    public long getTime() {
        return time;
    }

    public IMMessage setTime(long time) {
        this.time = time;
        return this;
    }

    public int getOnline() {
        return online;
    }

    public IMMessage setOnline(int online) {
        this.online = online;
        return this;
    }

    public String getSender() {
        return sender;
    }

    public IMMessage setSender(String sender) {
        this.sender = sender;
        return this;
    }

    public String getReceiver() {
        return receiver;
    }

    public IMMessage setReceiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    public String getContent() {
        return content;
    }

    public IMMessage setContent(String content) {
        this.content = content;
        return this;
    }

    public String getTerminal() {
        return terminal;
    }

    public IMMessage setTerminal(String terminal) {
        this.terminal = terminal;
        return this;
    }
}
